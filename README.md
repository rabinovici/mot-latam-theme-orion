# Motorola Latam Theme Atlas

--------------------------------------------------------------------------------

## Quick Start:

- `npm install` - This will install all the JS dependencies the project needs.
- `gulp dev` - Run and **start coding**.

--------------------------------------------------------------------------------

## Requirements

- Nodejs > 14.x
- Gulp > 4.x

You need Node.js and npm on your system and install some dependencies.

### Node.js (Comes with NPM)

NPM comes directly with Node.js when you install it. Go to this link to download and follow instructions: <https://nodejs.org/en/download/>

To check if you have node and npm installed, open your terminal and type:

```bash
$ node -v
$ npm -v
```

--------------------------------------------------------------------------------

## Gulp Tasks

### Main tasks:

- `gulp` : the default task that start devel process.

- `gulp dev` : browserSync opens the project in your default browser and live reloads when changes are made on your files (gulp watch + gulp serve).

### Util tasks:

- `gulp watch` : watch for changes are made (won't open the browser).

- `gulp serve` : browserSync opens the project in your default browser.

--------------------------------------------------------------------------------

## JS and CSS (inject in html)

If you want to include a local library or a **js** file make sure use the `inline` attribute.

- **JS:**

```html
<script src="./vendors/bootstrap/bootstrap.min.js" inline></script>
```

- Same with **css** files.

```html
<link rel="stylesheet" href="./vendors/bootstrap/bootstrap.min.css" inline>
```

## Include

You can use include files, this is very helpful when you need reuse same code.

https://www.npmjs.com/package/gulp-file-include

```html
@@include('./include/view.html')
```

--------------------------------------------------------------------------------

## Preview:

![preview](preview.jpg)
