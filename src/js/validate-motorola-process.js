/* global $, fetch, console */
/* eslint no-undef: "error", semi: 2 */
/* jshint esversion: 6 */

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Validate - Get Redirect Url - thank you page
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

var $host = window.location.href;
var $keywords;
var $locale;

if (!$host.includes('motorolasolutions')) {
  console.log('%c --- Debug Mode ---', 'color: #d00000');

  /** ::::::: Validate Meta Tag - Keywowrds */
  $(document).ready(function () {
    if ($('meta[name=keywords]').length) {
      $keywords = $('meta[name=keywords]').attr('content');
    } else {
      $keywords = 'NO-META-TAG';
    }

    if ($keywords === 'CHANGE_THIS' || $keywords === 'NO-META-TAG' || $keywords.length === 0) {
      $keywords = '!!! ERROR !!!: No Meta Tag or Meta Keywords Defined';
    } else {
      $keywords = $('meta[name="keywords"]').attr('content');
    }

    /** ::::::: Validate Meta Tag - Locale */
    if ($('meta[name=locale]').length) {
      $locale = $('meta[name=locale]').attr('content');
    } else {
      $locale = 'NO-META-TAG';
    }

    if ($locale === 'CHANGE_THIS' || $locale === 'NO-META-TAG' || $locale.length === 0) {
      $locale = '!!! ERROR !!!: No Meta Tag or Meta Locale Defined - Add correct value Ex: en_us';
    } else {
      $locale = $('meta[name="locale"]').attr('content');
    }
  });

  /**
   * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
   * ·······  Console Messages
   * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
   */

  var $hiddenFields;

  $(document).ready(function () {
    if (!$('input[name="activity-id"]').length) {
      $hiddenFields = '!!! ERROR !!!: No ID Field - Please add hidden input field';
    }
    if (!$('input[name="redirect-url"]').length) {
      $hiddenFields = '!!! ERROR !!!: No Redirect Field - Please add hidden input field';
    }
    if (!$('input[name="activities-url-referral"]').length) {
      $hiddenFields = '!!! ERROR !!!: No Input Field for UTMs - Please add hidden input field';
    } else {
      $hiddenFields = 'OK';
    }

    if ($redirectUrl.indexOf('ERROR') !== -1) {
      console.log('%c --- Meta Redirect Url         : ' + $redirectUrl, 'background: #ebff00; color: #d00000');
    } else {
      console.log('%c --- Meta Redirect Url         : ' + $redirectUrl, 'color: #0c6b00');
    }

    if ($redirectUrlId.indexOf('ERROR') !== -1) {
      console.log('%c --- Meta Redirect Url + ID    : ' + $redirectUrlId, 'background: #ebff00; color: #d00000');
    } else {
      console.log('%c --- Meta Redirect Url + ID    : ' + $redirectUrlId, 'color: #0c6b00');
    }

    if ($keywords.indexOf('ERROR') !== -1) {
      console.log('%c --- Meta Keywords             : ' + $keywords, 'background: #ebff00; color: #d00000');
    } else {
      console.log('%c --- Meta Keywords             : ' + $keywords, 'color: #0c6b00');
    }

    if ($locale.indexOf('ERROR') !== -1) {
      console.log('%c --- Meta Locale               : ' + $locale, 'background: #ebff00; color: #d00000');
    } else {
      console.log('%c --- Meta Locale               : ' + $locale, 'color: #0c6b00');
    }

    if ($('form').attr('id') === 'CHANGE_THIS') {
      console.log('%c --- : !!! ERROR !!!: Set form id', 'background: #ebff00; color: #d00000');
    }
    if ($('form').attr('name') === 'CHANGE_THIS') {
      console.log('%c --- : !!! ERROR !!!: Set form name', 'background: #ebff00; color: #d00000');
    }
    if ($('[name="elqFormName"]').val() === 'CHANGE_THIS' || !$('[name="elqFormName"]').length || !$('[name="elqSiteID"]').length) {
      console.log('%c --- : !!! ERROR !!!: Set Eloqua hidden field', 'background: #ebff00; color: #d00000');
    }
    if ($('[type="IN/Form2"]').attr('data-form') === 'CHANGE_THIS_FORM_ID') {
      console.log('%c --- : !!! ERROR !!!: Set LinkedIn Integration', 'background: #ebff00; color: #d00000');
    }

    if ($hiddenFields.indexOf('ERROR') !== -1) {
      console.log('%c --- Input Hidden Fields       : ' + $hiddenFields, 'background: #ebff00; color: #d00000');
    } else {
      console.log('%c --- Input Hidden Fields       : ' + $hiddenFields, 'color: #0c6b00');
    }

  });

}
