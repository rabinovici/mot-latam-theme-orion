/**
 * ::::::: Industries
*/

var $selectIndustry = '' +
'<option value=""> Please Select </option>' +
'<option value="Education">Education</option>' +
'<option value="Fire and Emergency Medical Services">Fire and Emergency Medical Services</option>' +
'<option value="Healthcare">Healthcare</option>' +
'<option value="Hospitality and Retail">Hospitality and Retail</option>' +
'<option value="Manufacturing">Manufacturing</option>' +
'<option value="Mining, Oil and Gas">Mining, Oil and Gas</option>' +
'<option value="National Government Security">National Government Security</option>' +
'<option value="Police">Police</option>' +
'<option value="Public Services (non-Public Safety Government)">Public Services (non-Public Safety Government)</option>' +
'<option value="Telecommunications, Finance, and General Management Services">Telecommunications, Finance, and General Management Services</option>' +
'<option value="Transportation and Logistics">Transportation and Logistics</option>' +
'<option value="Utilities">Utilities</option>' +
'<option value="Motorola">Motorola</option>' +
'<option value="Partner">Partner</option>';

/**
 * ::::::: Countries
*/
var $selectCountry = '' +
  '<option value=""> Please Select </option>'+
  '<option value="AI">Anguilla</option>'+
  '<option value="AG">Antigua and Barbuda</option>'+
  '<option value="AW">Aruba</option>'+
  '<option value="BS">Bahamas</option>'+
  '<option value="BB">Barbados</option>'+
  '<option value="BZ">Belize</option>'+
  '<option value="Curacao">Curacao</option>'+
  '<option value="KY">Cayman Islands</option>'+
  '<option value="DM">Dominica</option>'+
  '<option value="GF">French Guiana</option>'+
  '<option value="GP">Guadeloupe</option>'+
  '<option value="HT">Haiti</option>'+
  '<option value="JM">Jamaica</option>'+
  '<option value="MS">Montserrat</option>'+
  '<option value="KN">Saint Kitts and Nevis</option>'+
  '<option value="Saint Martin">Saint Martin</option>'+
  '<option value="LC">Saint Lucia</option>'+
  '<option value="VC">St. Vincent and Grenadines</option>'+
  '<option value="Saint Barthelemy">Saint Barthelemy</option>'+
  '<option value="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option>'+
  '<option value="SR">Suriname</option>'+
  '<option value="TT">Trinidad and Tobago</option>'+
  '<option value="TC">Turks/Caicos Isls.</option>'+
  '<option value="VI">Virgin Islands (U.S.)</option>';



/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Privacy Policy
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/


var $privacyPolicy = 'Yes, I would like to receive updates, product news and promotional materials from Motorola Solutions. Please review our <a href="https://www.motorolasolutions.com/en_us/about/privacy-policy.html" target="_blank" data-uet="{\'link-type\':\'link\',\'link-label\':\'privacy policy\',\'page-area\':\'2\'}">Privacy statement</a> for more details.';

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Submit btn
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/

var $submintBtn = 'SUBMIT';
