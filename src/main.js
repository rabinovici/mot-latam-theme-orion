/* global $ */
/* eslint no-undef: "error", semi: 2 */
/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Menu - Hamburger https://jonsuh.com/hamburgers/
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

var $hamburger = $('.hamburger');

$hamburger.on('click', function (e) {
  $hamburger.toggleClass('is-active');
});

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Menu - Collpase on click
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

$(function () {
  var navMain = $('.navbar-collapse');
  var $hamburger = $('.hamburger');

  navMain.on('click', 'a', null, function () {
    navMain.collapse('hide');
    $hamburger.toggleClass('is-active');
  });
});

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Menu - Add Active State
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

$('.js-menu-active a').click(function (e) {
  e.preventDefault();
  $('a').removeClass('active');
  $(this).addClass('active');
});
