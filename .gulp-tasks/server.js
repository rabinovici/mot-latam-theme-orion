/* eslint semi: "error" */
/* jshint node: true */
/* jshint esversion: 6 */

'use strict';

const {
  src,
  dest,
  parallel,
  series,
  watch,
  gulp
} = require('gulp');

/* */
const log = console.log.bind(console);
/* */
const browserSync = require('browser-sync');
const connect = require('gulp-connect-php');

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Server
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 **/

function serverHtml () {
  browserSync.init({
    port: 8888,
    ui: false,
    open: true,
    server: {
      baseDir: './.tmp/',
      directory: true
    }
  });
}

function serverPhp () {
  connect.server({
    base: './.tmp/',
    port: 8099,
    keepalive: true
  }, browserProxy());
}

function browserProxy () {
  browserSync.init({
    port: 9999,
    open: true,
    proxy: '127.0.0.1:8099'
  });
}

function reload (done) {
  browserSync.reload();
  done();
}

module.exports = {
  serverHtml,
  serverPhp,
  browserProxy,
  reload,
}
