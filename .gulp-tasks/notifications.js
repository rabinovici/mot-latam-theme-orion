/* eslint semi: "error" */
/* jshint node: true */
/* jshint esversion: 6 */

'use strict';

const {
  src,
  dest,
  parallel,
  series,
  watch,
  gulp
} = require('gulp');

/* */
const log = console.log.bind(console);
/* */
const colors = require('colors');

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Notifications
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 **/

function notifyDone (done) {
  log(colors.white.bold.bgRed('           ::::::: DONE :::::::              '));
  done();
}

function notifyDoneHtml (done) {
  log(colors.black.bold.bgGreen('           ::::::: DONE html :::::::              '));
  done();
}

function notifyDonePhp (done) {
  log(colors.black.bold.bgGreen('           ::::::: DONE php :::::::              '));
  done();
}

function notifyDoneCss (done) {
  log(colors.white.bold.bgMagenta('           ::::::: DONE css :::::::            '));
  done();
}

function notifyDoneJs (done) {
  log(colors.black.bold.bgCyan('           ::::::: DONE js :::::::            '));
  done();
}

function notifyDoneJson (done) {
  log(colors.black.bold.bgYellow('           ::::::: DONE json :::::::            '));
  done();
}

function notifyDoneImg (done) {
  log(colors.black.bold.bgBlue('           ::::::: Sync Images :::::::            '));
  done();
}

function notifyDoneLocal (done) {
  log(colors.black.bold.bgYellow('                                                      '));
  log(colors.magenta.bold.bgYellow('          Files ready on ' + colors.black.bgYellow('build-local/ ') + 'folder          '));
  log(colors.black.bold.bgYellow('                                                      '));
  done();
}

function notifyDoneFiles (done) {
  log(colors.black.bold.bgYellow('                                                      '));
  log(colors.magenta.bold.bgYellow('          Files ready on ' + colors.black.bgYellow('build-files/ ') + 'folder          '));
  log(colors.black.bold.bgYellow('                                                      '));
  done();
}

function notifyDoneDist (done) {
  log(colors.yellow(`
          XNNNNNNNNNNNNNNNNNNXd/
          Xd                  yX/
          Xd                  yX:+X/
          Xd                  yX/.-yX/
          Xd                  +hhhhhhhNX
          Xd                          XN
          Xd         /h   +s.         XN
          Xd       oXy-    +Xh-       XN
          Xd     oNs.        /dd-     XN
          Xd     oXy.        /Xh-     XN
          Xd       /Xh-    oXy.       XN
          Xd         :y   /o          XN
          Xd                          XN
          Xd                          XN
          Xd                          XN
          Xd                          XN
          Xd                          XN
          XNNNNNNNNNNNNNNNNNNNNNNNNNNMDQ
  `));
  log('          ' + colors.yellow('Dist DONE' + '\n'));
  done();
}

module.exports = {
  notifyDone,
  notifyDoneHtml,
  notifyDonePhp,
  notifyDoneCss,
  notifyDoneJs,
  notifyDoneJson,
  notifyDoneImg,
  notifyDoneLocal,
  notifyDoneFiles,
  notifyDoneDist,
}
