/* eslint semi: "error" */
/* jshint node: true */
/* jshint esversion: 6 */

'use strict';

const {
  src,
  dest,
  parallel,
  series,
  watch,
  gulp
} = require('gulp');

/* */
const log = console.log.bind(console);
/* */
const fs = require('fs-extra');
const shell = require('gulp-shell');

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  Extras
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

/**
 * ::::::: Hidden Dist
 */
function hiddenDist () {
  var $file = './dist/.hidden';

  return fs.outputFile($file, '');
}

/**
 * ::::::: Shell Compress Zip
 */

function zip () {
  return src('./')
    .pipe(shell([
      // Create compress folder
      'mkdir -p compress',
      // Delete old zip files
      'find compress/ -name "*.zip" -type f -delete',
      //
      'COMMIT_HASH=$(git log -1 --pretty=tformat:%h) && ' +
      'touch hash-$COMMIT_HASH.txt',
      //
      'COMMIT_HASH=$(git log -1 --pretty=tformat:%h) && ' +
      ' FOLDER_NAME=$(basename "$PWD") &&' +
      'cp -r dist  $FOLDER_NAME-hash-$COMMIT_HASH',
      // Create zip files
      'COMMIT_HASH=$(git log -1 --pretty=tformat:%h) && ' +
      ' FOLDER_NAME=$(basename "$PWD") &&' +
      ' zip -r compress/$FOLDER_NAME-hash-$COMMIT_HASH.zip hash-$COMMIT_HASH.txt $FOLDER_NAME-hash-$COMMIT_HASH && ' +
      //
      'COMMIT_HASH=$(git log -1 --pretty=tformat:%h) && ' +
      ' FOLDER_NAME=$(basename "$PWD") &&' +
      ' rm -rf $FOLDER_NAME-hash-$COMMIT_HASH && rm hash-$COMMIT_HASH.txt',
      'if $(git diff-index --quiet HEAD --);' +
      'then echo "$(tput setaf 7)\n Files in $(tput setaf 3)compress/$(tput setaf 7) folder";' +
      'else echo "$(tput setaf 1)$(tput setab 7)\n WARNING: \n ' +
      'Commit your changes first and run $(tput setaf 8)gulp compress $(tput setaf 1)again.$(tput sgr 0)\n"; fi'
    ]));
}

module.exports = {
  hiddenDist,
  zip,
}
