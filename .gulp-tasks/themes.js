/* eslint semi: "error" */
/* jshint node: true */
/* jshint esversion: 6 */

'use strict';

const {
  src,
  dest,
} = require('gulp');

/* */
const log = console.log.bind(console);
/* */
const colors = require('colors');
const del = require('del');
const fs = require('fs-extra');
const path = require('path');
const prompt = require('gulp-prompt');

const _del = require('./del');

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ······· Themes
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/

/**
 * ::::::: Backup SRC Folder
 */
function backup () {
  var today = new Date();
  var year = today.getFullYear();
  var month = ('0' + today.getMonth()).slice(-2);
  var day = ('0' + today.getDate()).slice(-2);
  var hour = ('0' + today.getHours()).slice(-2);
  var minute = ('0' + today.getMinutes()).slice(-2);
  var seconds = ('0' + today.getSeconds()).slice(-2);

  var dataStamp = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + seconds;

  return src('./src/**/*')
    .pipe(dest('./.backups/' + dataStamp));
}

/**
 * ::::::: Backup Theme Folder
 */
function backupTheme (theme) {
  var today = new Date();
  var year = today.getFullYear();
  var month = ('0' + today.getMonth()).slice(-2);
  var day = ('0' + today.getDate()).slice(-2);
  var hour = ('0' + today.getHours()).slice(-2);
  var minute = ('0' + today.getMinutes()).slice(-2);
  var seconds = ('0' + today.getSeconds()).slice(-2);

  var dataStamp = year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + seconds;

  return new Promise(function(resolve) {
    src('./themes/' + theme + '/**/*')
      .pipe(
        dest('./.backups/themes/' + theme + '_' + dataStamp)
          .on('finish', function(){
            resolve(theme);
          })
      );
  })
}

function getFolders (dir) {
  return fs.readdirSync(dir)
    .filter(function (file) {
      return fs.statSync(path.join(dir, file)).isDirectory();
    });
}

/**
 * ::::::: Copy Theme
 */
function copyTheme (theme) {
  return new Promise(function(resolve) {
    del('./src/.hidden');
    src([
      './themes/' + theme + '/**/*'
    ], { dot: true })
      .pipe(dest('./src/').on('finish', function(){
        resolve(theme);
      }));
  })
}

/**
 * ::::::: Copy Src to Theme
 */
function copySrcToTheme (theme) {
  return new Promise(function(resolve) {
    src(['./src/**/*'], { dot: true })
      .pipe(dest('./themes/' + theme + '/').on('finish', function(){
        resolve(theme);
      }));
  })
}

/**
 * ::::::: Save Theme
 */
function saveTheme() {
  var $themeList = getFolders('./themes');
  $themeList.unshift('- Exit -');

  return new Promise(function(resolve) {
    src('./')
      .pipe(prompt.prompt({
        type: 'list',
        name: 'theme',
        message: colors.yellow('\n Please select your ' + colors.red('destination theme') + ':'),
        choices: $themeList
      }, ($res) => {
        if ($res.theme === '- Exit -') {
          console.log(colors.red('\n   ⊗ No theme selected\n'));
        } else {
          console.log('\n  ✔ Theme selected: ' + colors.green($res.theme));
          
          backupTheme($res.theme).then(function(themeName) {
            copySrcToTheme(themeName).then(function(data){
              resolve(console.log('\n  ✔ Theme ' + colors.green(data) + ' saved!'));
              _del.delSrc();
              fs.outputFile('./src/.hidden', '');
              resolve(console.log(colors.green('\n  ✔ Now commit your changes.')));
            });
          });
        }
      }));
  })
}  

/**
 * ::::::: Theme
 */

function theme() {
  var $themeList = getFolders('./themes');
  $themeList.unshift('- Exit -');

  return new Promise(function(resolve) {
    src('./')
      .pipe(prompt.prompt({
        type: 'list',
        name: 'theme',
        message: colors.yellow('\n Please select your ' + colors.red('theme') + ':'),
        choices: $themeList
      }, ($res) => {
        if ($res.theme === '- Exit -') {
          console.log(colors.red('\n   ⊗ No theme selected\n'));
        } else {
          console.log('\n  ✔ Theme selected: ' + colors.green($res.theme));
          _del.delSrc();
          copyTheme($res.theme).then(function(data){
            resolve(console.log('\n  ✔ Theme created: ' + colors.green(data)));
          });
        }
      }));
  })
}  

module.exports = {
  backup,
  theme,
  saveTheme,
}
