/* eslint semi: "error" */
/* jshint node: true */
/* jshint esversion: 6 */

'use strict';

const {
  src,
  dest,
  parallel,
  series,
  watch,
  gulp
} = require('gulp');

/* */
const log = console.log.bind(console);
/* */
const babel = require('gulp-babel');
const jshint = require('gulp-jshint');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const stylish = require('jshint-stylish');
const beautifyCode = require('gulp-beautify-code');
const strip = require('gulp-strip-comments');

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  JS
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

function onErrorJs (err) {
  notify.onError({
    title: 'Code JS Error',
    message: '\n' + err + '\n',
    sound: 'Beep'
  })(err);
  this.emit('end');
}

function compileJs () {
  return src([
    './src/*.js',
    './src/js/*.js'
  ], { base: './src/' })
    .pipe(plumber({ errorHandler: onErrorJs }))
    .pipe(babel())
    .pipe(strip())
    .pipe(jshint({
      esversion: 6
    }))
    .pipe(jshint.reporter(stylish))
    .pipe(beautifyCode({
      indent_size: 2,
      indent_level: 0,
      indent_style: 'space',
      indent_char: ' ',
      indent_with_tabs: false,
      quotes: 'single',
      align_assignments: true,
      'preserve-newlines': false
    }))
    .pipe(dest('./.tmp/'));
}

module.exports =  { compileJs };
