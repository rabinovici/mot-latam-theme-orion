'sass.js'
/* eslint semi: "error" */
/* jshint node: true */
/* jshint esversion: 6 */

'use strict';

const {
  src,
  dest,
  parallel,
  series,
  watch,
  gulp
} = require('gulp');

/* */
const log = console.log.bind(console);
/* */
const autoprefixer = require('autoprefixer');
const beautifyCode = require('gulp-beautify-code');
const duplicates = require('postcss-discard-duplicates');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const postcss = require('gulp-postcss');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const unique = require('postcss-unique-selectors');
const pfm = require('postcss-font-magician');
const cssnano = require('cssnano');

/**
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 * ·······  SASS
 * ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
 */

function onErrorSass (err) {
  var rexUrl = /[^\\/]+$/;
  var pattern = rexUrl.exec(err.file);
  notify.onError({
    title: 'Code Error',
    message: `
  code ERROR
  File    :  ${pattern}
  Line    :  ${err.line}
  Message :  ${err.messageOriginal}
  `,
    sound: 'Beep'
  })(err);
  this.emit('end');
}

var postcssPlugins = [
  autoprefixer(
    'last 2 version',
    '> 1%',
    'ie 8',
    'ie 9',
    'ie 11',
    'ios 6',
    'android 4'
  ),
  // pfm(),
  cssnano({
    discardComments: true
  }),
  unique(),
  duplicates()
];

function compileSass () {
  return src([
    './src/*.scss',
    './src/sass/*.scss'
  ], { base: './src/' })
    .pipe(plumber({ errorHandler: onErrorSass }))
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(postcss(postcssPlugins))
    .pipe(sourcemaps.write('./'))
    .pipe(dest('./.tmp/'));
}

function beautifyCss () {
  return src([
    './.tmp/*.scss',
    './.tmp/sass/*.scss'
  ], { base: './.tmp/' })
    .pipe(beautifyCode({
      indent_size: 2,
      indent_level: 0,
      indent_style: 'space',
      indent_char: ' ',
      indent_with_tabs: false,
      'preserve-newlines': false
    }))
    .pipe(dest('./.tmp/'));
}

module.exports = {
  compileSass,
  beautifyCss
}
